/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPTIMIZER_SGD_H_
#define AIDGE_CORE_OPTIMIZER_SGD_H_

#include <functional>
#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/learning/optimizer/Optimizer.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

enum class SGDAttr {
    Momentum,
    Dampening
};

class SGD: public Optimizer, public StaticAttributes<SGDAttr, float, float> {
private:
    std::vector<Tensor> mGradientInertia;
    Tensor mLR{std::vector<std::size_t>({1})};
    Tensor mMomentum{std::vector<std::size_t>({1})};
    Tensor mReversedDampening{std::vector<std::size_t>({1})};

public:
    using Attributes_ = StaticAttributes<SGDAttr, float, float>;
    template <SGDAttr e>
    using attr = typename Attributes_::template attr<e>;

    SGD(const float momentum = 0.0f, const float dampening = 0.0f)
        : Optimizer(),
          Attributes_(attr<SGDAttr::Momentum>(momentum),
                    attr<SGDAttr::Dampening>(dampening))
    {
        mMomentum.setBackend("cpu");
        mMomentum.set<float>(0, momentum);
        mReversedDampening.setBackend("cpu");
        mReversedDampening.set<float>(0, 1.0f - dampening);
    }

    void update() override final {
        mLR.setBackend(mParameters[0]->getImpl()->backend());
        mLR.set<float>(0, learningRate());
        if (mParameters[0]->getImpl()->backend() != mMomentum.getImpl()->backend()) {
            mMomentum.setBackend(mParameters[0]->getImpl()->backend());
            mReversedDampening.setBackend(mParameters[0]->getImpl()->backend());
        }

        if (mLRScheduler.step() == 0) {
            for (std::size_t i = 0; i < mParameters.size(); ++i) {
                mGradientInertia[i] = mParameters[i]->grad()->clone();
                *mParameters[i] = *mParameters[i] - mLR*mGradientInertia[i];
            }
        } else {
            for (std::size_t i = 0; i < mParameters.size(); ++i) {
                mGradientInertia[i] = mMomentum*mGradientInertia[i] + mReversedDampening*(*mParameters[i]->grad());
                *mParameters[i] = *mParameters[i] - mLR*mGradientInertia[i];
            }
        }
        mLRScheduler.update();
    }

    void setParameters(const std::vector<std::shared_ptr<Tensor>>& parameters) override final {
        Optimizer::setParameters(parameters);
        mGradientInertia = std::vector<Tensor>(parameters.size());
        for (std::size_t i = 0; i < parameters.size(); ++i) {
            mGradientInertia[i] = Tensor(parameters[i]->dims());
        }
    }
};

} // namespace Aidge


namespace {
template <>
const char *const EnumStrings<Aidge::SGDAttr>::data[] = {
    "Momentum",
    "Dampening"
};
}
#endif // AIDGE_CORE_OPTIMIZER_SGD_H_
