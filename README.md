# aidge_learning

In this module, you can find functions and classes to train your models:

- ``Optimizer`` (SGD)
- ``LRScheduler`` (ConstantLR, StepLR)
- loss functions (MSE)

## Dependencies

- aidge_core
- aidge_backend_cpu

## Python installation

```python
pip install . -v
```

## C++ installation

```bash
./setup.sh -m core -m backend_cpu -m learning --release
```
