/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <numeric>  // std::iota

#include "aidge/backend/cpu/operator/PowImpl.hpp"
#include "aidge/backend/cpu/operator/ReduceMeanImpl.hpp"
#include "aidge/backend/cpu/operator/SubImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/loss/LossList.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Pow.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/operator/Sub.hpp"
#include "aidge/recipes/GraphViewHelper.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"

Aidge::Tensor Aidge::loss::MSE(std::shared_ptr<Tensor>& prediction,
                               const std::shared_ptr<Tensor>& target) {
    /*
    Implementation note:
    MSE is computed using a graph in order to not be backend dependant.

    The graph used is the following:

    pred->Sub
    label->Sub
    Sub->Pow
    (2)->Pow->ReduceMean->Loss
    Sub->Mul
    (2/NbBatch)->Mul->Gradient
    */

    prediction->initGrad(); // Enable gradient for output

    // compile_gradient(graph);  // Warning compile gradient here, without
    //                           // it, grad is nullptr. Maybe we can find a better
    //                           // place to do so ?

    AIDGE_ASSERT(target->dims().size() == 2,
                 "Label must have two dims: [BatchSize, NbChannel]");

    std::shared_ptr<Tensor> outputGrad = prediction->grad();

    AIDGE_ASSERT(prediction->backend() == target->backend(),
                 "'prediction' and 'target' Tensors must be on the "
                 "same backend. Found {} and {}.\n",
                 prediction->backend(), target->backend());
    AIDGE_ASSERT(prediction->dims() == target->dims(),
                 "'prediction' (shape {}) and 'target' (shape {}) Tensors must "
                 "have the same dimensions.\n",
                 prediction->dims(), target->dims());
    AIDGE_ASSERT(prediction->dataType() == target->dataType(),
                 "'prediction' (data type {}) and 'target' (data type {}) "
                 "Tensors must have the same data type.\n",
                 prediction->dataType(), target->dataType());

    // could be accelerated with constexpr constructors
    std::vector<int> axes_dims(prediction->nbDims());
    std::iota(std::begin(axes_dims), std::end(axes_dims), 0);
    auto rm_node = ReduceMean(axes_dims, 1, "mse_res");

    const std::shared_ptr<Node> pow_node = Pow("square");
    const std::shared_ptr<Node> pow_exp_node =
        Producer(std::make_shared<Tensor>(Array1D<int, 1>{{2}}), "exp_val");
    pow_exp_node->addChild(pow_node, 0, 1);

    const std::shared_ptr<Node> sub_node = Sub("err");
    Producer(prediction, "pred")->addChild(sub_node, 0, 0);
    Producer(target, "label")->addChild(sub_node, 0, 1);

    const std::shared_ptr<Node> mul_node = Mul("gradient");

    // Note: this assume target is [nbBatch, nbChan]
    Producer(std::make_shared<Tensor>(
                 Array1D<float, 1>{{2 / float(target->dims()[0])}}))
        ->addChild(mul_node, 0, 1);
    sub_node->addChild(mul_node, 0, 0);  // Error computation branch !

    std::shared_ptr<GraphView> gv_local =
        Sequential({sub_node, pow_node, rm_node});
    gv_local->add({sub_node->getParent(0), sub_node->getParent(1), pow_exp_node,
                   mul_node->getParent(1), mul_node});
    gv_local->compile(prediction->getImpl()->backend(), prediction->dataType());

    SequentialScheduler ss_local{gv_local};
    ss_local.forward(false);

    // Retrieve gradient
    // Can we avoid copy ?
    outputGrad->copyFrom(
        std::dynamic_pointer_cast<OperatorTensor>(mul_node->getOperator())
            ->getOutput(0)
            ->clone());

    // TODO: way too complicated to access
    const std::shared_ptr<OperatorTensor> res =
        std::dynamic_pointer_cast<OperatorTensor>(rm_node->getOperator());
    return res->getOutput(0)->clone();
}
