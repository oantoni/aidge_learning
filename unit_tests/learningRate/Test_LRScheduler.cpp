/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cstddef>  // std::size_t
// #include <memory>
#include <random>   // std::random_device, std::mt19937, std::uniform_int_distribution
#include <vector>

// #include "aidge/data/Tensor.hpp"
#include "aidge/learning/learningRate/LRScheduler.hpp"
#include "aidge/learning/learningRate/LRSchedulerList.hpp"

namespace Aidge {
TEST_CASE("[learning/LR] Construction & evolution", "[LRScheduler]") {
    constexpr std::uint16_t NBTRIALS = 10;

    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> nbStepsDist(1, 1000);
    std::uniform_real_distribution<float> initValDist(1.0e-7f, 1.0f);

    SECTION("ConstantLR") {
        for (std::size_t trial = 0; trial < NBTRIALS; ++trial) {
            const std::size_t nbSteps = nbStepsDist(gen);

            // truth
            const float truth = initValDist(gen);

            // create learning rate scheduler
            LRScheduler myLR = learning::ConstantLR(truth);

            // prediction
            std::vector<float> profile = myLR.lr_profiling(nbSteps);

            // learning rate computation
            std::size_t step = 0;
            for (; (step < nbSteps) && (truth == profile[step]) && (truth == myLR.learningRate()); ++step) {
                myLR.update();
            }

            REQUIRE(step == nbSteps);
        }
    }

    SECTION("StepLR") {
        std::uniform_int_distribution<std::size_t> stepSizeDist(1, 100);
        std::uniform_real_distribution<float> gammaDist(0.0001f, 1.0f);

        for (std::size_t trial = 0; trial < NBTRIALS; ++trial) {
            const float initialLR = initValDist(gen);
            const std::size_t nbSteps = nbStepsDist(gen);
            const float gamma = gammaDist(gen);
            const std::size_t stepSize = stepSizeDist(gen);
            LRScheduler myLR = learning::StepLR(initialLR, stepSize, gamma);

            // truth
            std::vector<float> truth(nbSteps);
            truth[0] = initialLR;
            for (std::size_t i = 1; i < nbSteps; ++i) {
                truth[i] = (i % stepSize == 0) ? truth[i - 1] * gamma : truth[i - 1];
            }

            // profiling
            std::vector<float> profile = myLR.lr_profiling(nbSteps);

            // learning rate computation
            std::size_t step = 0;
            for (; (step < nbSteps) && (truth[step] == profile[step]) && (truth[step] == myLR.learningRate()); ++step) {
                myLR.update();
            }

            REQUIRE(step == nbSteps);
        }
    }
}
} // namespace Aidge
