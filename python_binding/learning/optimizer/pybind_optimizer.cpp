/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <memory>

#include "aidge/learning/optimizer/Optimizer.hpp"

namespace py = pybind11;
namespace Aidge {
// namespace learning {

void init_Optimizer(py::module& m) {
    py::class_<Optimizer, std::shared_ptr<Optimizer>>(m, "Optimizer")
    .def(py::init<>())
    .def("parameters", &Optimizer::parameters)
    .def("set_parameters", &Optimizer::setParameters)
    .def("learning_rate", &Optimizer::learningRate)
    .def("learning_rate_scheduler", &Optimizer::learningRateScheduler)
    .def("set_learning_rate_scheduler", &Optimizer::setLearningRateScheduler)
    .def("reset_grad", &Optimizer::resetGrad)
    .def("update", &Optimizer::update);
}
// }  // namespace learning
}  // namespace Aidge
